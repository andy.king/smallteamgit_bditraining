# Introduction

We are going to use this simple markdown document to practice the steps of creating a merge request on GitLab (aka "pull" request on GitHub).

# Jammy shortbread

## Ingredients

* 200g Hard butter, at room temp
* 100g Caster sugar
* 300g Plain flour 
* 100g Ground almonds  
* Pinch of salt (if using unsalted butter)
* 50g Flaked almonds
* 350g jam/curd

## Method 

Grease and line an 18x28cm rectangular tin. 

Preheat oven to 180&deg;C (Gas Mark 4).

Beat the butter and sugar until combined, then mix in the flour and ground almonds. The mixture should be loose and crumbly.
(If you forgot to let the butter soften to room temperature then beat the cold butter on its own until it softens before adding the sugar, otherwise it'll spray sugar all over your kitchen!)
(If you'd rather, you can mix by hand - rub the butter into the dry ingredients like making pastry)

Tip about 3/4 of the mixture into the lined baking tray and press down firmly. 

Spread the jam on top evenly (leave a small border around the edge or it'll bubble up the sides and caramelise).

Add the flaked almonds to the reserved mixture and scatter on top of the jam.

Bake for 35-45mins, until the top is light golden. Cover with a sheet of baking paper partway if it's getting too brown.

Remove from the oven and leave to cool in the tin for a bit, then transfer to a wire rack, slice and enjoy!
